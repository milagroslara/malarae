/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.examen2t1.model;

/**
 *
 * @author Sistemas-02
 */
public class Empleado {
    String Primer_Nombre;
    String segundo_Nombre;
    String Primer_Apellido;
    String Segundo_Apellido;
    int edad;
    int fecha_de_contratacion;
    int fecha_de_nacimiento;
    int Años_de_experiencia;
    String Oficio;

    public String getPrimer_Nombre() {
        return Primer_Nombre;
    }

    public void setPrimer_Nombre(String Primer_Nombre) {
        this.Primer_Nombre = Primer_Nombre;
    }

    public String getSegundo_Nombre() {
        return segundo_Nombre;
    }

    public void setSegundo_Nombre(String segundo_Nombre) {
        this.segundo_Nombre = segundo_Nombre;
    }

    public String getPrimer_Apellido() {
        return Primer_Apellido;
    }

    public void setPrimer_Apellido(String Primer_Apellido) {
        this.Primer_Apellido = Primer_Apellido;
    }

    public String getSegundo_Apellido() {
        return Segundo_Apellido;
    }

    public void setSegundo_Apellido(String Segundo_Apellido) {
        this.Segundo_Apellido = Segundo_Apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getFecha_de_contratacion() {
        return fecha_de_contratacion;
    }

    public void setFecha_de_contratacion(int fecha_de_contratacion) {
        this.fecha_de_contratacion = fecha_de_contratacion;
    }

    public int getFecha_de_nacimiento() {
        return fecha_de_nacimiento;
    }

    public void setFecha_de_nacimiento(int fecha_de_nacimiento) {
        this.fecha_de_nacimiento = fecha_de_nacimiento;
    }

    public int getAños_de_experiencia() {
        return Años_de_experiencia;
    }

    public void setAños_de_experiencia(int Años_de_experiencia) {
        this.Años_de_experiencia = Años_de_experiencia;
    }

    public String getOficio() {
        return Oficio;
    }

    public void setOficio(String Oficio) {
        this.Oficio = Oficio;
    }
    
    
}
